﻿// TODO: implement class Vehicle.
//       Properties: Id (string), VehicleType (VehicleType), Balance (decimal).
//       The format of the identifier is explained in the description of the home task.
//       Id and VehicleType should not be able for changing.
//       The Balance should be able to change only in the CoolParking.BL project.
//       The type of constructor is shown in the tests and the constructor should have a validation, which also is clear from the tests.
//       Static method GenerateRandomRegistrationPlateNumber should return a randomly generated unique identifier.
using System;
using System.Text;
using System.Text.RegularExpressions;

namespace CoolParking.BL.Models
{
    public class Vehicle
    {
        public string Id { get; }
        public VehicleType VehicleType { get; }
        public decimal Balance { get; internal set; }

        public Vehicle(string id, VehicleType vehicleType, decimal balance)
        {
            Regex rx = new Regex("^[A-Z]{2}-[0-9]{4}-[A-Z]{2}");

            if(balance < 0) throw new ArgumentException();
            if (!rx.IsMatch(id)) throw new ArgumentException();

            Id = id;
            VehicleType = vehicleType;
            Balance = balance;
        }

        public static string GenerateRandomRegistrationPlateNumber()
        {
            Random rnd = new Random();

            StringBuilder sb = new StringBuilder();
            sb.Append((char)rnd.Next('A', 'Z'));
            sb.Append((char)rnd.Next('A', 'Z'));
            sb.Append("-");
            sb.Append(rnd.Next(0, 9));
            sb.Append(rnd.Next(0, 9));
            sb.Append(rnd.Next(0, 9));
            sb.Append(rnd.Next(0, 9));
            sb.Append("-");
            sb.Append((char)rnd.Next('A', 'Z'));
            sb.Append((char)rnd.Next('A', 'Z'));

            return sb.ToString();
        }
    }
}