﻿// TODO: implement class Settings.
//       Implementation details are up to you, they just have to meet the requirements of the home task.

using System.Collections.Generic;

namespace CoolParking.BL.Models
{
    public static class Settings
    {
        readonly public static decimal parkingStartBalance = 0m;
        readonly public static int parkingCapacity = 10;
        readonly public static int paymentWithdrawPeriod = 5000; // ms
        readonly public static int logWritePeriod = 60000; // ms
        readonly public static decimal coefficientPenalty = 2.5m;

        readonly public static Dictionary<string, decimal> tariffPrices = new Dictionary<string, decimal>()
        {
            {"PassengerCar", 2.0m},
            {"Truck", 5.0m},
            {"Bus", 3.5m},
            {"Motorcycle", 1.0m}
        };
    }
}
