﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CoolParking.BL.Services;
using CoolParking.BL.Models;
using System.Text.RegularExpressions;

namespace CoolParking.BL.CI
{
    public static class Menu
    {
        public static void ShowParkingBalance(ParkingService ps)
        {
            Console.WriteLine("Balance: {0}", ps.GetBalance());
        }

        public static void ShowEarnedMoneyInCurrentPeriod(ParkingService ps)
        {
            decimal money = 0;

            foreach (var t in ps.GetLastParkingTransactions())
                money += t.withdrawnMoney;

            Console.WriteLine("Earned money: {0}", money);
        }

        public static void ShowAmountOfFreeSpotsOnParking(ParkingService ps)
        {
            int free = ps.GetFreePlaces();
            int notFree = ps.GetCapacity() - free;

            Console.WriteLine($"Spots Free\\Not free: {free}\\{notFree}");
        }

        public static void ShowTransactionsInCurrentPeriod(ParkingService ps)
        {
            Console.WriteLine("Transactions in current period\n\n");
            StringBuilder transactionsInfo = new StringBuilder();

            foreach (var ti in ps.GetLastParkingTransactions())
            {
                transactionsInfo.Append($"Time: {ti.dateTime}\n");
                transactionsInfo.Append($"Vehicle Id: {ti.vehicleID}\n");
                transactionsInfo.Append($"Withdrawn money: {ti.withdrawnMoney}\n\n");
            }

            Console.WriteLine(transactionsInfo.ToString());
        }

        public static void ShowTransactionHistory(ParkingService ps)
        {
            try
            {
                Console.WriteLine("Transaction history\n\n");
                Console.Write(ps.ReadFromLog());
            }
            catch(InvalidOperationException ex)
            {
                Console.Write(ex.Message);
            }
        }

        public static void ShowVehiclesOnParking(ParkingService ps)
        {
            Console.WriteLine("List of vehicle on parking");

            StringBuilder vehiclesInfo = new StringBuilder();

            foreach (var v in ps.GetVehicles())
            {
                vehiclesInfo.Append($"Id: {v.Id}\n");
                vehiclesInfo.Append($"Balance: {v.Balance}\n");
                vehiclesInfo.Append($"Vehicle type: {v.VehicleType.ToString()}\n\n");
            }

            Console.WriteLine(vehiclesInfo.ToString());
        }

        public static void AddVehicleOnParking(ParkingService ps)
        {
            Regex rx = new Regex("^[A-Z]{2}-[0-9]{4}-[A-Z]{2}");
            Console.Write("Enter vehicle Id(XX-YYYY-XX)(X: A-Z, Y: 0-9): ");
            string id = Console.ReadLine();
            while(!rx.IsMatch(id))
            {
                Console.WriteLine("Invalid vehicle ID");
                Console.Write("Please, enter again: ");
                id = Console.ReadLine();
            }

            Console.Write("Enter vehicle type(0 - PassengerCar, 1 - Truck, 2 - Bus, 3 - Motorcycle): ");
            string vehicleTypeInput = Console.ReadLine();
            int vehicleType = Int32.TryParse(vehicleTypeInput, out _) ? Int32.Parse(vehicleTypeInput) : -1;
            while(vehicleType == -1 || (vehicleType < 0 || vehicleType > 3))
            {
                Console.WriteLine("Invalid vehicle type");
                Console.Write("Please, enter again: ");
                vehicleTypeInput = Console.ReadLine();
                vehicleType = Int32.TryParse(vehicleTypeInput, out _) ? Int32.Parse(vehicleTypeInput) : -1;
            }

            Console.Write("Enter vehicle Balance(>=0): ");
            string balanceInput = Console.ReadLine();
            int balance = Int32.TryParse(balanceInput, out _) ? Int32.Parse(balanceInput) : -1;
            while (balance == -1 || balance < 0)
            {
                Console.WriteLine("Invalid vehicle balance");
                Console.Write("Please, enter again: ");
                balanceInput = Console.ReadLine();
                balance = Int32.TryParse(balanceInput, out _) ? Int32.Parse(balanceInput) : -1;
            }

            try
            {
                var vehicle = new Vehicle(id, (VehicleType)vehicleType, balance);
                ps.AddVehicle(vehicle);

                Console.WriteLine("Vehicle is on parking");
            }
            catch (InvalidOperationException ex)
            {
                Console.WriteLine(ex.Message);
            }
            catch (ArgumentException ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        public static void RemoveVehicleFromParking(ParkingService ps)
        {
            Regex rx = new Regex("^[A-Z]{2}-[0-9]{4}-[A-Z]{2}");
            Console.Write("Enter vehicle Id(XX-YYYY-XX)(X: A-Z, Y: 0-9): ");
            string id = Console.ReadLine();
            while (!rx.IsMatch(id))
            {
                Console.WriteLine("Invalid vehicle ID");
                Console.Write("Please, enter again: ");
                id = Console.ReadLine();
            }

            try
            {
                ps.RemoveVehicle(id);
                Console.WriteLine("Vehicle has been removed");
            }
            catch(ArgumentException ex)
            {
                Console.WriteLine(ex.Message);
            }
            catch(InvalidOperationException ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        public static void TopUpVehicleBalance(ParkingService ps)
        {
            Regex rx = new Regex("^[A-Z]{2}-[0-9]{4}-[A-Z]{2}");
            Console.Write("Enter vehicle Id(XX-YYYY-XX)(X: A-Z, Y: 0-9): ");
            string id = Console.ReadLine();
            while (!rx.IsMatch(id))
            {
                Console.WriteLine("Invalid vehicle ID");
                Console.Write("Please, enter again: ");
                id = Console.ReadLine();
            }


            Console.Write("Enter sum to top up(>=0): ");
            string sumInput = Console.ReadLine();
            decimal sum = Decimal.TryParse(sumInput, out _) ? Decimal.Parse(sumInput) : -1;
            while (sum == -1)
            {
                Console.WriteLine("Invalid sum");
                Console.Write("Please, enter again: ");
                sumInput = Console.ReadLine();
                sum = Decimal.TryParse(sumInput, out _) ? Decimal.Parse(sumInput) : -1;
            }

            try
            {
                ps.TopUpVehicle(id, sum);
                Console.WriteLine("Balance has been toped up");
            }
            catch (ArgumentException ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
    }
}
